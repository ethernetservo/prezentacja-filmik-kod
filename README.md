# README #

Stwórz aplikację pomiarową pozwalającą na komunikację z wykorzystaniem standardu Ethernet (protokół TCP/IP). Praca nad zadaniem projektowym będzie wymagała wykorzystania zestawu Arduino + Etherent W5100 Shield + Motor Shield + Serwomechanizm.

Projekt dotyczy zbudowania urządzenia umożliwiającego symulację wychyłu ramienia robota.

Wymagania projektowe:

stworzenie komunikacji pomiędzy urządzeniami
opracowanie mechanizmu prezentacji modelu na stronie WWW
opracowanie mechanizmu wysyłania danych pomiarowych z Arduino do serwera aplikacyjnego (request POST)
stworzenie komunikacji z serwerem aplikacyjnym przez WebService REST (dropwizzard)
prezentacja stopni wychyłu ramienia
możliwość sterowania ramieniem z poziomu WWW
definiowanie programów odtwarzających ruchy symulowanego ramienia
Elementy sprzętowe:

http://smartrobots.pl/arduino-leonardo-klon
http://smartrobots.pl/arduino/shieldy/arduino-ethernet-shield
http://smartrobots.pl/arduino/shieldy/arduino-motor-shield
http://smartrobots.pl/silniki/serwomechanizm-MG90S