
#include <SPI.h>
#include <Ethernet.h>

#include <Servo.h>
Servo myservo;  // create servo object to control a servo

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; //physical mac address
byte ip[] = { 192, 168, 1, 170 }; // ip in lan
byte gateway[] = { 192, 168, 1, 1 }; // internet access via router
byte subnet[] = { 255, 255, 255, 0 }; //subnet mask
EthernetServer server(80); //server port

String readString;
int angle;
String link = "<img src='http://i.imgur.com/HsC9uzC.png' border=0></a><br/>";



//////////////////////

void setup(){

  pinMode(5, OUTPUT); //pin selected to control
  //start Ethernet
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();

  //myservo.write(90); //set initial servo position if desired
  myservo.attach(7);  //the pin for the servo co
  //enable serial data print
  Serial.begin(9600);
  Serial.println("server LED test 1.0"); // so I can keep track of what is loaded
}

void printpage(EthernetClient client){
  client.println("HTTP/1.1 200 OK"); //send new page
          client.println("Content-Type: text/html");
          client.println();

          client.println("<HTML>");
          client.println("<HEAD>");
          client.println("<TITLE>Arduino GET test page</TITLE>");
          client.println("</HEAD>");
          client.println("<BODY>");

          client.println("<H1>Kurewsko dobre servo</H1>");
         
          // DIY buttons
          client.println("<a href=\"/?0\"\">0</a>");
          client.println("<a href=\"/?20\"\">20</a>");    
          client.println("<a href=\"/?40\"\">40</a>");
          client.println("<a href=\"/?60\"\">60</a>"); 
          client.println("<a href=\"/?80\"\">80</a>");
          client.println("<a href=\"/?100\"\">100</a>"); 
          client.println("<a href=\"/?120\"\">120</a>");
          client.println("<a href=\"/?140\"\">140</a>");  
          client.println("<a href=\"/?160\"\">160</a>");
          client.println("<a href=\"/?180\"\">180</a><br />");  
          client.println("<a href=\"/?+\"\">+</a>");  
          client.println("<a href=\"/?-\"\">-</a><br />"); 

          client.print(link);
          
           
          client.println("</BODY>");
          client.println("</HTML>");
  }

void loop(){
  // Create a client connection
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        

        //read char by char HTTP request
        if (readString.length() < 100) {

          //store characters to string
          readString += c;
          //Serial.print(c);
        }

        //if HTTP request has ended
        if (c == '\n') {

          ///////////////
          Serial.println(readString); //print to serial monitor for debuging

//         printpage(client);
//
//          delay(1);
//          //stopping client
//          client.stop();

          ///////////////////// control arduino pin
          if(readString.indexOf("0") >0)//checks for on
          {
            link = "<img src='http://i.imgur.com/yKaYL5H.png' border=0></a><br/>";
            angle = 0;
            myservo.write(angle);
          //  printpage(client);
          }
          if(readString.indexOf("20") >0)//checks for off
          {
            link = "<img src='http://i.imgur.com/jats5tg.png' border=0></a><br/>";
            angle = 20;
            myservo.write(angle);
         //   printpage(client);
          }
          if(readString.indexOf("40") >0)//checks for on
          {
            link = "<img src='http://i.imgur.com/8Kffjzh.png' border=0></a><br/>";
            angle = 40;
            myservo.write(angle);
          }
          if(readString.indexOf("60") >0)//checks for off
          {
            link = "<img src='http://i.imgur.com/H2Nd45s.png' border=0></a><br/>";
            angle = 60;
            myservo.write(angle);
          }
          if(readString.indexOf("80") >0)//checks for on
          {
            link = "<img src='http://i.imgur.com/u7gvvMD.png' border=0></a><br/>";
            angle = 80;
            myservo.write(angle);
          }
          if(readString.indexOf("100") >0)//checks for off
          {
            link = "<img src='http://i.imgur.com/bXmONjN.png' border=0></a><br/>";
            angle = 100;
            myservo.write(angle);
          }
          if(readString.indexOf("120") >0)//checks for on
          {
            link = "<img src='http://i.imgur.com/Llgdxpd.png' border=0></a><br/>";
            angle = 120;
            myservo.write(angle);
          }
          if(readString.indexOf("140") >0)//checks for off
          {
            link = "<img src='http://i.imgur.com/QU4oK12.png' border=0></a><br/>";
            angle = 140;
            myservo.write(angle);
          }
          if(readString.indexOf("160") >0)//checks for on
          {
            link = "<img src='http://i.imgur.com/npc4R9V.png' border=0></a><br/>";
            angle = 160;
            myservo.write(angle);
          }
          if(readString.indexOf("180") >0)//checks for off
          {
            link = "<img src='http://i.imgur.com/wzTT7Pq.png' border=0></a><br/>";
            angle = 180;
            myservo.write(angle);
          }
          if(readString.indexOf("+") >0)//checks for +
          {
            
            //angle = 10;
            angle=angle+10;
            myservo.write(angle);
            if(angle == 20){
              link = "<img src='http://i.imgur.com/jats5tg.png' border=0></a><br/>";
              } 
              if(angle == 40){
                link = "<img src='http://i.imgur.com/8Kffjzh.png' border=0></a><br/>";
                }
              if(angle == 60){
                link = "<img src='http://i.imgur.com/H2Nd45s.png' border=0></a><br/>";
                }
                if(angle == 80){
              link = "<img src='http://i.imgur.com/u7gvvMD.png' border=0></a><br/>";
              } 
              if(angle == 100){
                link = "<img src='http://i.imgur.com/bXmONjN.png' border=0></a><br/>";
                }
                if(angle == 120){
              link = "<img src='http://i.imgur.com/Llgdxpd.png' border=0></a><br/>";
              } 
              if(angle == 140){
                link = "<img src='http://i.imgur.com/QU4oK12.png' border=0></a><br/>";
                }
                if(angle == 160){
              link = "<img src='http://i.imgur.com/npc4R9V.png' border=0></a><br/>";
              } 
              if(angle == 180){
                link = "<img src='http://i.imgur.com/wzTT7Pq.png' border=0></a><br/>";
                }
          }
          if(readString.indexOf("-") >0)//checks for +
          {
            //angle = 10;
            angle=angle-10;
            myservo.write(angle);
            if(angle == 0){
              link = "<img src='http://i.imgur.com/yKaYL5H.png' border=0></a><br/>";
              } 
            if(angle == 20){
              link = "<img src='http://i.imgur.com/jats5tg.png' border=0></a><br/>";
              } 
              if(angle == 40){
                link = "<img src='http://i.imgur.com/8Kffjzh.png' border=0></a><br/>";
                }
              if(angle == 60){
                link = "<img src='http://i.imgur.com/H2Nd45s.png' border=0></a><br/>";
                }
                if(angle == 80){
              link = "<img src='http://i.imgur.com/u7gvvMD.png' border=0></a><br/>";
              } 
              if(angle == 100){
                link = "<img src='http://i.imgur.com/bXmONjN.png' border=0></a><br/>";
                }
                if(angle == 120){
              link = "<img src='http://i.imgur.com/Llgdxpd.png' border=0></a><br/>";
              } 
              if(angle == 140){
                link = "<img src='http://i.imgur.com/QU4oK12.png' border=0></a><br/>";
                }
                if(angle == 160){
              link = "<img src='http://i.imgur.com/npc4R9V.png' border=0></a><br/>";
              } 
              if(angle == 180){
                link = "<img src='http://i.imgur.com/wzTT7Pq.png' border=0></a><br/>";
                }
          }

                   printpage(client);

          delay(1);
          //stopping client
          client.stop();
          
          //clearing string for next read
          readString="";

        }
      }
    }
  }
} 
